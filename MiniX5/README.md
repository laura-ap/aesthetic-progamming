# MiniX5
###### Please run the code [here](https://aesthetic-progamming-laura-ap-480b211131f65acb83f176e1696bf281f.gitlab.io/MiniX5/index.html) <br>  Please view the full repository [here](https://gitlab.com/laura-ap/aesthetic-progamming/-/tree/main/MiniX5)


![](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExZW9oYzE3ZHJpbGpuangyMXhvM2FmYmppc2p5Nmh0ZjEwMDZ2dnNkOCZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/lYJ24uAf3Al0QfkiLU/giphy.gif)



**Rules:** <br>
1.)Draw lines from either top-left to bottom-right or top-right to bottom-left. <br>
2.)Randomly determine the color and strokeweight of lines. <br>
3.) Use Perlin noise to influence the y-position of lines

**Conditional statements:** <br> I have utilizesed conditional statements in my program, through the use of if-statements. <br> My first if-statement decides which direction to draw the diagonal line based on the result of ``random(1) < 0.5``. If the condition is true, it draws the line from top-left to bottom-right. If false, it draws the line from top-right to bottom-left. <br>

My second statement (``if (x > width)``) checks if the x coordinate exceeds the width of the canvas. If true, it resets x to 0 and increments the y coordinate by the value of spacing. Making the program 'move down', when a line hits the edge of the canvas

My third and last statement (``if (y > height)``) is used to stop the loop, when the y coordinate exceeds the height of the canvas.

**For-loops:** <br>
In my code, I have used a for-loop in the setup function to generate initial lines, and in the draw function to draw those lines.

**How does this MiniX help you to understand the
idea of “auto-generator”** <br>
The idea of "auto-generator" in programming involves the creation of dynamic content or behavior based on predefined rules and algorithms. <br>
In my MiniX I had a hard time with coming up with, and implementing my 'rules', but without them the code wouldnt work. <br>
I have used the ``noise()`` function to create a pseudo-randomness to the visual expression of the program. It has been implimented to add variation in y-position of the lines.
<br>

<br>
Refrences: <br>
·Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020 (chapter 5)
