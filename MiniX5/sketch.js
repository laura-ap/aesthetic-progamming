let x = 0;
let y = 0;
let spacing = 100;
let len = spacing; // lenght of lines

function setup() {
  createCanvas(windowWidth, windowHeight); // parameters creating a canvas that fills the window
  background(255);
}

function draw() {
  // generate random values for colors
  let r = random(255);
  let g = random(255);
  let b = random(255);
  stroke(r, g, b); // Set the stroke color to the random RGB values

  // Add randomness to line thickness
  let weight = random(1, 8); // Generate a random stroke weight between 1 and 8
  strokeWeight(weight); // Set the stroke weight


  //Uses a random condition to decide the direction of the diagonal line.
  if (random(1) < 0.5) { //he condition random(1) < 0.5 has a 50% probability of being true each time the code runs.
    line(x, y, x + len, y + len); //Draws a line from the current position diagonally down-right.
  } else {
    line(x, y + len, x + len, y); // Draws a line from the current position diagonally up-right.
  }

  x += spacing; // Move to the next x position by adding the spacing value

  // Add variation in y-position based on noise
  y += noise(x / 100) * 50 - 25; // Use Perlin noise to add randomness to the y position


  if (x > width) {
    x = 0; // Reset x to the left edge of the canvas
    y += spacing; // Move to the next row by adding the spacing value to y
  }

  // Add a boundary for y-position
  if (y > height) {
    noLoop(); // Stop drawing when y-position exceeds window height
  }
}

//Unlike random noise, which generates completely uncorrelated values, Perlin noise produces smoother, continuous transitions between values (pseudorandomness)
