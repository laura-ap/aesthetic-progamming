//The class handle jumping, moving, collision detection, and displaying the character.
//'this.' provides a way to access properties and methods of the object within its own context
class Model {
  constructor() { //A standard method for creating and initializing an object created with a class.
    this.r = 200 //used as size of character
    this.x = 50; // initial x position
    this.y = height - this.r; // initial y position, at the bottom of the canvas
    this.vy = 0; // initial velocity (attribute that controls change of position over time) on the y-axis, the character starts with no vertical movement.
    this.gravity = 2; //could be a global variable, but is only used on charater, gravity pulls down character
  }

//function is called when a jump action is triggered (spacebar)
  jump() {
    if (this.y == height - this.r) { //only allow jumps when at the bottom of canvas
      this.vy = -40; //setting velocity to move the character up the y axis, moving it 40px up the y axis
    }
  }

//Determine if the character (this) collides with a zombie (zombie).
  hits(zombie) {
    //A small offset added to the x- and y-coordinate for collision detection purposes.
    let x1 = this.x + this.r * 0.05; 
    let y1 = this.y + this.r * 0.05;
    let x2 = zombie.x + zombie.r * 0.05;
    let y2 = zombie.y + zombie.r * 0.05;

// check for collision between the model and the zombie using circular collision detection, checks if two circles intersect
    return collideCircleCircle(x1, y1, this.r, x2, y2, zombie.r);

  }

//Simulates the effect of gravity and updates the position, ensuring the character falls back down after jumping.
  move() {
    this.y += this.vy; // update y position based on velocity
    this.vy += this.gravity; // apply gravity to velocity
    this.y = constrain(this.y, 0, height - this.r); // constrain y position to stay within canvas bounds
  }

   // draw the character image at the current position with the specified size
  show() {
    image(cImg, this.x, this.y, this.r, this.r);

  }
}