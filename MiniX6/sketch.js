
let model;
let cImg; //character (model)
let oImg; //object (zombie)
let bImg; //background
let zombie = []; //Array to store multiple zombie objects

//preload images 
function preload() {
  cImg = loadImage('model.gif');
  oImg = loadImage('zombie.png');
  bImg = loadImage('background.gif');

}

function setup() {
 createCanvas(windowWidth,windowHeight);
 model = new Model(); //create a new instance of the Model class
}

 //Checks if the space key is pressed and makes the model jump.
function keyPressed() {
  if (key == ' '){
    model.jump();
  }
}

function draw() {

if(random(1) < 0.005) { // Randomly adds new zombies to the array + frequency of objects apperence
  zombie.push(new Zombie()); //add new zombie to the array
}
background(bImg); //draws the background image
for (let t of zombie) { //Loops through each zombie to move and display them, and checks for collisions with the model. 
  t.move(); //(dot notation,'t' serves as a placeholder for zombie-objects) move the zombie
  t.show(); //(dot notation) show the zombie
  if (model.hits(t)){ // check if the model hits the zombie
    textSize(120);
    textFont('Copperplate');
    textAlign(CENTER);
    fill(255,0,0);
    text('GAME OVER', width /2, height /2);
    textSize(35);
      fill(255);
      
    console.log('game over'); // log game over to console
    noLoop(); // stop the draw loop

  }
}

model.show(); //display the model
model.move(); //move the model

}
