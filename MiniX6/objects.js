// creating classes organizes related properties and methods together, making the code easier to understand and manage.

class Zombie {
  //initialize the zombie's properties
  constructor() { //A standard method for creating and initializing an object created with a class.
    this.r = 150; // used for sizing of zombie object
    this.x = width; // Initial x position, starting from the right edge of the canvas
    this.y = height - this.r; // Initial y position, at the bottom of the canvas minus the height of the zombie
  }

// Method to move the zombie leftward across the canvas
  move() {
    this.x -= 15; //pace of object, decrease the x position by 15 pixels, moving the zombie to the left
  }

// Method to display the zombie image at its current position
  show() {
    image(oImg, this.x, this.y, this.r, this.r); // Draw the zombie image at the current x and y position with the specified size
  }
}
