# MiniX6

###### Please run the code [here](https://aesthetic-progamming-laura-ap-480b211131f65acb83f176e1696bf281f.gitlab.io/MiniX6/index.html) <br>  Please view the full repository [here](https://gitlab.com/laura-ap/aesthetic-progamming/-/tree/main/MiniX6)

![](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExeGxnMjZkZm1nazlza3BxbTRrdjh1bTZvcWk1enJpMWZuNXEwbGtndiZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/T8gwKNLIXxJGdTgtc3/giphy.gif)


**Describe how does/do your game/game objects work?** <br>
 In my MiniX6, i have made a game inspired by the Chrome Dinosaur Game, where a dinosaur has to jump over cacti to collect point. The game ends when the dinosaur runs into a cactus. 
In my game I have two main objects: a 'model' and zombies. Like the Chrome Dinosaur Game, the model has to jump over the zombies, using the space bar. 


i have made the objects by creating classes in seperate files, and then referencing them in my sketch.js file.  


**Describe how you program the objects and their related attributes, and the methods in your game.** <br>
In the game, I utilize object-oriented programming to define the behavior and properties of the character (Model) and obstacles (Zombie). <br>

**Attributes:** The Model and Zombie classes has attributes like ``r`` (size), ``x`` (initial x position), ``y`` y (initial y position), ``vy`` (vertical velocity), and ``gravity``. These attributes define the character's position, size, and movement dynamics.

**Methods:** In the class 'Model', i have used the methods ``jump()``, ``hits(zombie)``, ``move()`` and ``show()`` defining the behavior of the character.
In the class 'Zombie', i also use the ``move()`` and ``show()`` method, with different attributes.

By using classes, you can encapsulate the properties and behaviors of both the character (model) and the objects (zombies), making the code modular and easier to manage. Each class has attributes to store the state and methods to define the behavior, ensuring a clear and organized structure. This approach leverages object-oriented programming principles, promoting maintainability and extensibility in the game's development.


**Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?** <br>
In this Mini X, we use object-oriented programming to abstract the complex details and operations of the game's elements into manageable classes and methods.

Object abstraction in programming is not just a technical necessity; it also mirrors broader cultural practices of abstraction and simplification. For example interfaces, where complex backend operations are abstracted, to make a more seamless user experience

By abstracting complex details and operations, we can focus on creating rich, interactive experiences

***References:*** <br>
·[Tutorial, The Coding Train](https://www.youtube.com/watch?v=l0HoJHc-63Q) <br>

·[p5.collide2D ](https://cdn.jsdelivr.net/gh/bmoren/p5.collide2D/p5.collide2d.min.js/) ``collideCircleCircle()`` <br>

·Soon Winnie & Cox, Geoff, "Object Abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164 - chapter 6.`
