# MiniX2

###### Please run the code [here](https://aesthetic-progamming-laura-ap-480b211131f65acb83f176e1696bf281f.gitlab.io/MiniX2/index.html) <br>  Please view the full repository [here](https://gitlab.com/laura-ap/aesthetic-progamming/-/tree/main/MiniX2)

![](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExZHU3cWl4dGVtdGY5cXJlcmNjZHlpNHNoOTMwcGs5cGJydzZ0a2t4cyZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/LydPnmmNAFk4wzqVYA/giphy.gif)


**Describe your program and what you have used and learnt**<br>
I had a hard time coming up with an idea for this weeks MiniX, because i wanted to create an emoji that could represent everyone. My solution to this was two yellow stars. One with an upward facing mouth, and one with a downward facing one.
<br>
I am aware that the upward and downward facing mouths, could symbolize a simplification of emotions - that there is only good or bad, happy or sad, etc. I was unsure how to visualize other emotions, and therefore chose to stick to these two emojis, since they are, after all, very universal.

I chose to make these personalized stars because these are some figures I often draw on paper. In addition, stars are something we all have some kind of relationship with, no matter where you are in the world. For me, stars are something that connects us, because we all live under them. <br>
I googled around a bit to see what stars symbolized for others, and found many different interpretations, but the one that stood out to me the most, was the one from the "Nationwide Children's Hospital", which is also the one I included in my program. With everything going on in the world i liked that the star was/is seen as a symbol for hope. <br>


I  used the ``triangle()`` p5.js function to make the stars, but to make the lips, i found a piece of code from Erika Noma's "Self-portrait in p5.js",  where she uses the ``arc()`` to make the curved silhouette, which i played around with until i found a result i was happy with.



**How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?** <br>
As mentioned, I used stars. I feel like these aren't a representation of anyone specific, but rather everyone. Besides that, I like how it looks like they have arms and legs. At first i actually wanted to make accesories for them, mostly because i thought it would be cute if they were wearing a butterfly, or a cowboy-hat. But by them being simple, i havn't assigned them any gender, race or identity (that i know of).


<br>
[Reference for Erika Noma's lips](https://medium.com/@erikanoma/self-portrait-in-p5-js-ac71e1755bec)
<br>
[Reference for Nationwide Children's Hospitals](https://www.onoursleeves.org/mental-wellness-tools-guides/icon-collection/star#:~:text=A%20star%20is%20a%20beacon,a%20magical%20and%20inspiring%20presence.)
