function setup() {
  createCanvas(1000, 1000);
  textSize(20);
  fill("#DB7093");
  text('A star is a beacon of hope, a shining light that guides the way.\nIts a symbol of positivity, happiness or renewal.\nLook up into the nighttime sky, and the stars have a magical and inspiring presence', 50, 400);
}

function draw() {
  background(255); // Optional: Clear the background each frame

  noStroke();
  fill("yellow");

  drawHappyStar(140, 120);
  drawSadStar(540, 120);

  fill("#DB7093");
  noStroke();
  arc(175, 240, 40, 20, 0, PI); // Happy star lips
}

function drawHappyStar(x, y) {
  triangle(x - 35, y - 75, x, y - 150, x + 35, y - 75);
  triangle(x - 90, y - 10, x - 35, y - 75, x - 35, y + 5);
  triangle(x + 90, y - 10, x + 35, y - 75, x + 35, y + 5);
  triangle(x + 50, y + 35, x - 10, y + 55, x + 100, y + 155);
  triangle(x + 75, y + 55, x - 15, y + 55, x - 65, y + 155);
  rect(x - 35, y - 75, 75, 65);

  fill("black");
  circle(x - 10, y - 75, 10);
  circle(x + 10, y - 75, 10);
}

function drawSadStar(x, y) {
  fill("yellow");
  triangle(x - 35, y - 75, x, y - 150, x + 35, y - 75);
  triangle(x - 90, y + 30, x - 35, y - 75, x - 35, y + 5);
  triangle(x + 90, y + 30, x + 35, y - 75, x + 35, y + 5);
  triangle(x + 50, y + 35, x - 10, y + 55, x + 100, y + 155);
  triangle(x + 75, y + 55, x - 15, y + 55, x - 65, y + 155);
  rect(x - 35, y - 75, 75, 65);

  fill("black");
  circle(x - 10, y - 75, 10);
  circle(x + 10, y - 75, 10);

  // Sad lips
  translate(0, -100);
  fill("#e38fab");
  noStroke();
  arc(x + 35, y + 130, 20, 10, PI, 0);
}

//The code is divided into smaller, manageable functions (drawHappyStar and drawSadStar).
//The code is more readable, with each function handling a specific task.