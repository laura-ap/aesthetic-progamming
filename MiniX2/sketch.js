function setup() {
  // put setup code here
  createCanvas(1000, 1000);
  textSize(20);
  fill("#DB7093");
  text ('A star is a beacon of hope, a shining light that guides the way.\nIts a symbol of  positivity, happiness or renewal.\nLook up into the nighttime sky, and the stars have a magical and inspiring presence', 50, 400);

}

function draw() {

                                   
    noStroke()
    fill("yellow");

    //happy star
    triangle(140, 120, 175, 45, 210, 120);
    triangle(50, 110, 140, 120, 140, 200);
    triangle(350, 110, 210, 120, 210, 200);
    triangle(225, 155, 115, 175, 310, 310);
    triangle(250, 175, 125, 175, 75, 310);
    rect(140, 120, 75, 65);

    fill("black");
    circle(165, 120, 10);
    circle(185, 120, 10);

    //sad star
    fill("yellow");
    triangle(540, 120, 575, 45, 610, 120);
    triangle(450, 150, 540, 120, 540, 200);
    triangle(750, 150, 610, 120, 610, 200);
    triangle(625, 155, 515, 175, 710, 310);
    triangle(650, 175, 525, 175, 475, 310);
    rect(540, 120, 75, 65);

    //eyes
    fill("black");
    circle(565, 120, 10);
    circle(585, 120, 10);

    //lips
    translate(0, -100);
    fill("#e38fab");
    noStroke();
    arc(575, 250, 20, 10, PI, 0);

    fill("#DB7093");
    noStroke();
    arc(175, 240, 40, 20, 0, PI);

  }