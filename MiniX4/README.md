# MiniX4
###### Please run the code [here](https://aesthetic-progamming-laura-ap-480b211131f65acb83f176e1696bf281f.gitlab.io/MiniX4/index.html) <br>  Please view the full repository [here](https://gitlab.com/laura-ap/aesthetic-progamming/-/tree/main/MiniX4)

#### Im pretty sure the RUNME code doesnt work, but I need help with that:( - until then you can look at my gif!

![](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExN3N3dDljNTgwZWFoMXB5cmh5cW12NWpndHhrdnd3MWdqdjI3eWw4MyZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/Xl1zxI0twKpA1lkXEj/giphy-downsized-large.gif)


**A title for and a short description of your work (1000 characters or less) as if you were going to submit it to the festival.**
<br> For my MiniX4, i have created "catcalling or complimenting?" (title in progress). I had a hard time coming up with an idea for the subject "Data capture", without it being about how we are always watched. - Besides that, i really wanted to use the capture method for my project. 
<br> When I thought about this "always being watched" thing, I thought of catcalling because it's one of those times in the real world, where you can really feel watched. An unwanted opinion or 'compliment' being shouted at you. Something that can come across as one thing to someone seeing it or saying it, but be taken very differently by the person it’s directed at (The Line) - much like a comment section on social media!



**Your program and what you have used and learnt**
<br> I used the capture and the ctracker, which was new for me. But besides that i just created a ``rect()`` for the "scanner", and made it stop after 5 seconds, by making an if-statement. 
To make the 'popup'-banners i made an array. The popups are also just rectangles.

<br>
I btw found my "cat-calls" on The Line, who have an interesting article about the subject.
<br>
<br>
[Reference for The Line article](https://www.theline.org.au/the-secret-meanings-of-common-cat-calls/)

<br>
<br>
Other refrences: <br>
·ChatGPT <br>
·Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020


