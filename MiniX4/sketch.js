let capture;
let ctracker;
let scanning = false;
let popups = [];
let popupTexts = [
  "You look like you need sleep",
  "Smile! You will look prettier",
  "Hey sexy, why don’t you want to talk to me? I’m a good guy.",
  "C’mon sweetie, give me a smile!",
  "Aww c’mon, why you getting all offended? I’m paying you a compliment!"
];

function setup() {
  createCanvas(640, 480);
  noStroke();
  
  // Webcam capture
  capture = createCapture(VIDEO);
  capture.size(640, 480);
  capture.hide();

  // Face tracker setup
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);
  
  // Create a button for starting the scanning
  let button = createButton('Start Scanning for a compliment!');
  button.position(width/2 - 50, height + 10);
  button.mousePressed(startScanning);
}

function draw() {
  // Draw the captured video on the canvas
  image(capture, 0, 0, 640, 480);
  
  // Check if scanning is active
  if (scanning) {
    // Get current face positions
    let positions = ctracker.getCurrentPosition();
    
    // Check if face positions are available
    if (positions.length > 0) {
      // Draw the scanning bar on the face
      let barY = map(sin(frameCount * 0.05), -1, 1, 0, positions[7][1] - positions[33][1]);
      fill(0, 255, 0); // Green color
      rect(positions[33][0] - 100, positions[33][1] + barY, 200, 5);
    }
    
    // Stop scanning after 5 seconds
    if (frameCount > 5 * 60) { // 5 seconds * 60 frames per second
      scanning = false;
      showPopups();
    }
  }
  
  // Draw the popups
  for (let i = popups.length - 1; i >= 0; i--) {
    let popup = popups[i];
    popup.draw();
    popup.update();
    if (popup.closed) {
      popups.splice(i, 1);
    }
  }
}

// Function to start scanning
function startScanning() {
  scanning = true;
  popups = []; // Reset the popups array
}

// Function to show popups
function showPopups() {
  for (let i = 0; i < popupTexts.length; i++) {
    let x = random(width - 340) + 20; // Random x position within canvas with 20px padding
    let y = random(height - 190) + 20; // Random y position within canvas with 20px padding
    let popup = new Popup(x, y, popupTexts[i]);
    popups.push(popup);
  }
}

// Popup class
class Popup {
  constructor(x, y, text) {
    this.x = x;
    this.y = y;
    this.text = text;
    this.width = 300;
    this.height = 150;
    this.closed = false;
  }
  
  // Method to draw the popup
  draw() {
    // Draw the background rectangle
    fill(255);
    rect(this.x, this.y, this.width, this.height);
    
    // Draw the text
    fill(0);
    textAlign(CENTER, CENTER);
    text(this.text, this.x + 10, this.y + 10, this.width - 20, this.height - 20);
    
    // Draw close button
    fill(255, 0, 0);
    rect(this.x + this.width - 20, this.y, 20, 20);
    fill(255);
    textSize(14);
    textAlign(CENTER, CENTER);
    text('X', this.x + this.width - 10, this.y + 10);
  }
  
  // Method to update the popup
  update() {
    // Check if mouse is over the close button
    if (mouseX > this.x + this.width - 20 && mouseX < this.x + this.width && mouseY > this.y && mouseY < this.y + 20) {
      // Close the popup when clicked
      if (mouseIsPressed) {
        this.closed = true;
      }
    }
  }
}
