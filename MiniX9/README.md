# MiniX9
For my MiniX9 i decided to make a flowchart over my MiniX6, the zombie game. I did this because it was my most interactive MiniX.

![Flowchart over MiniX 6](MiniX9/Flowchart_MiniX9.jpg)

**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?** <br>
Flowcharts are valuable tools for visualizing processes and algorithms, but they can also present some challenges e.g Managing the complexity while keeping the flowchart clear and understandable, and subjectivity, Interpreting a flowchart can sometimes be subjective, as different individuals may have different interpretations of the symbols and logic used.