# MiniX3

###### Please run the code [here](https://aesthetic-progamming-laura-ap-480b211131f65acb83f176e1696bf281f.gitlab.io/MiniX3/index.html) <br>  Please view the full repository [here](https://gitlab.com/laura-ap/aesthetic-progamming/-/tree/main/MiniX3)

![](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExb244NG9xYjR0czZxZWVyNjd1em1xaW9mc3NnYnMxdmJsYWJvaXZtbiZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/ixBSJUjn9AJCyqGv8M/giphy.gif)

For this MiniX i created a optical illusion. **Look at the red dot, for 10 seconds then look away**, and experience how you are drawn into another world.

**– What do you want to explore and/or express?** <br>
With this Mini X, i wanted to create a optical illusion. <br> In the book, the throbber icon is described as a metaphor for waiting and anticipation in digital interactions. Throbbers are common in digital interfaces, signaling to users that a process is ongoing. <br> When looking in to the meaning of the throbber icon, and while reading the chapter, I kept on coming back to this idea that 'computer-time' is an illusion. Therefor i wanted to create an actual optical illusion, emphasizing that behind the symbol a process is ongoing. <br>

**– What are the time-related syntaxes/functions that you have used in your 
program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)? <br>**
The ``draw()`` function itself is central to the concept of time in p5.js, as it repeatedly executes the code within it, creating an animation frame by frame. <br> Inside of this function, i have made a ``for-loop``. This loop is used to draw each of the bars that make up the throbber. The loop ensures that each bar is positioned and rotated correctly around the center of the canvas. <br>
I have used the variable ``angle`` to control the rotation angle of the bars in the throbber animation. It starts with an initial value (0 in this case) and is incremented in each frame to create a spinning effect. <br>

In computational terms, time is constructed through loops and incremental updates. The draw() function runs continuously, with each iteration representing a moment in time. This continuous loop, combined with the incremental angle change, creates the perception of movement and time progression.

**– Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?**
Throbbers are common in digital interfaces, signaling to users that a process is ongoing. For example, on platforms like YouTube or Facebook, throbbers appear while content loads or updates. These icons serve both a functional and psychological purpose, indicating that the system is working and that the user should wait.
However, the throbber also hides the reasons for delays, offering no specific information about the cause or duration, and may obscure underlying inefficiencies or issues within the system.

<br>
refrences: <br>
·Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020 (chapter 3)