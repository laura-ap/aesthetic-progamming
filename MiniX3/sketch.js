let angle = 0;

function setup() {
  createCanvas(windowWidth, windowHeight);
  rectMode(CENTER); // Set rectangle mode to center
}

function draw() {
  background(0);
  translate(width / 2, height / 2); // Move the origin to the center of the canvas
  
  let numBars = 15; // Number of bars in the throbber
  let barLength = 75; // Length of each bar

   // Draw the static center dot
   fill(255,0,0); // Set fill color to black
   noStroke(); // Remove any stroke
   circle(0, 0, 10); // Draw the dot at the center

  for (let i = 0; i < numBars; i++) {
    push(); 
    rotate(TWO_PI / numBars * i + angle); // Rotate based on the number of bars and angle
    fill(250); // Set fill color to black
    rect(0, -barLength, 10, barLength); // Draw the bar
    pop(); 
  }

  angle += 0.03; // Increment angle for spinning effect
}

// This function is called whenever the window is resized
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}



