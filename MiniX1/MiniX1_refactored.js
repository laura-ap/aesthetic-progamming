function setup() {
    createCanvas(1000, 1000);
  }
  
  let angle = 0;
  
  // Colors (declaring variables using const, holding color values)
  const carColor = [255, 40, 0];
  const windowColor = [0, 51, 102];
  const tireColor = "black";
  const logoColor = "yellow";
  const rimColor = "grey";
  const mirrorColor = [196, 21, 28];
  const strokeColor = [200, 50, 28];
  
  function draw() {
    background(220);
    drawCarBody();
    drawCarWindows();
    drawCarDetails();
    drawTires();
    drawSpinningRims(175, 325, 145, 320); // Front rim
    drawSpinningRims(565, 325, 535, 320); // Back rim
    drawLogo();
  }
  
  function drawCarBody() {
    noStroke();
    fill(carColor);
    rect(220, 250, 265, 100); // bottomBody
    rect(290, 195, 200, 55, 5, 10, 0, 0); // top, rounded corners
    rect(10, 290, 210, 60, 10, 0, 0, 10); // frontEnd, rounded corners
    rect(480, 235, 190, 115, 0, 0, 50, 0); // backRoundedCorner
    triangle(20, 290, 175, 235, 175, 290); // frontEnd
    triangle(490, 200, 670, 235, 490, 235); // backEnd
    triangle(175, 250, 290, 250, 290, 195); // windShield
    square(175, 250, 45); // frontEnd
    triangle(215, 235, 175, 230, 175, 250); // front end fill
  }
  
  function drawCarWindows() {
    stroke("black");
    fill(windowColor);
    quad(475, 240, 470, 205, 400, 200, 390, 245); // backWindow
    quad(295, 210, 395, 200, 385, 245, 295, 245); // mainWindow
    triangle(210, 245, 290, 210, 290, 245); // frontWindow
  }
  
  function drawCarDetails() {
    fill(carColor);
    stroke(strokeColor);
    strokeWeight(1);
    circle(510, 230, 30);
    fill(mirrorColor);
    square(225, 225, 25, 10, 0, 0, 10); // mirror
    line(275, 300, 30, 300);
    line(550, 340, 30, 340);
    line(245, 255, 240, 340);
    line(385, 255, 370, 340);
  }
  
  function drawTires() {
    noStroke();
    fill(tireColor);
    circle(175, 325, 90); // front tire
    circle(565, 325, 90); // back tire
  }
  
  function drawSpinningRims(x1, y1) {
    push(); // saves the current drawing state
    translate(x1, y1); // Move to the center of the rim
    rotate(-angle); // Rotate by the current angle
    fill(rimColor);
    rect(-7.5, -27.5, 15, 55, 10); // Vertical bar
    rect(-27.5, -7.5, 55, 15, 10); // Horizontal bar
    pop(); // Restore the drawing state
    angle += 0.03; // Increment angle for spinning effect
  }
  
  function drawLogo() {
    fill(logoColor);
    circle(175, 325, 10); // front tire
    circle(565, 325, 10); // back tire
  }
  

//Modularization: The code is divided into separate functions for drawing different parts of the car

//Constants for Colors: Constants are used for colors to make the code more readable and easier to update.

//Improved Comments: Comments are added to explain the purpose of each function and major blocks of code.

//Simplified Function Parameters: The drawSpinningRims function now only takes the center coordinates for each rim, simplifying its usage.