function setup() {
  // put setup code here
  createCanvas(1000, 1000);
}

let angle = 0;

function draw() {

  noStroke()
  fill(255, 40, 0)
  //body of car//
  rect(220, 250, 265, 100) //rect(bottomBody)//
  rect(290, 195, 200, 55, 5, 10, 0, 0) //rect(top), rounded right and left top corner//
  rect(10, 290, 210, 60, 10, 0, 0, 10) //rect(frontEnd), rounded 'front' corners//
  rect(480, 235, 190, 115, 0, 0, 50, 0) //rect(backRoundedCorner)//
  triangle(20, 290, 175, 235, 175, 290); //triangle(frontEnd)//
  triangle(490, 200, 670, 235, 490, 235); //triangle (backEnd)//
  triangle(175, 250, 290, 250, 290, 195); //triangle(windSheild)//
  square(175, 250, 45); //square (frontEnd)//
  triangle(215, 235, 175, 230, 175, 250); //triangle(fill), front end)

  //windows and details//
  stroke("black");
  fill(0, 51, 102)
  quad(475, 240, 470, 205, 400, 200, 390, 245); //trapez(backWindow)//
  quad(295, 210, 395, 200, 385, 245, 295, 245);//trapez(mainWindow)//
  triangle(210, 245, 290, 210, 290, 245); //triangle(frontWindow)//


  fill(255, 40, 0)

  stroke(200, 50, 28)
  circle(510, 230, 30);
  strokeWeight(1);
  fill(196, 21, 28)
  stroke(200, 50, 28)
  square(225, 225, 25, 10, 0, 0, 10); //mirror//


  line(275, 300, 30, 300);
  line(550, 340, 30, 340);
  line(245, 255, 240, 340);
  line(385, 255, 370, 340);

  noStroke()
  fill("black");
  circle(175, 325, 90); //front tire//
  circle(565, 325, 90); //back tire//


  // drawSpinningRims function called
  drawSpinningRims(175, 325, 145, 320); // Front rim
  drawSpinningRims(565, 325, 535, 320); // Back rim

  //logo//
  fill("yellow");
  circle(175, 325, 10); //front tire//
  circle(565,325,10) //back tire//

}

function drawSpinningRims(x1, y1, x2, y2) {
  push(); // saves the current drawing state
  
  translate(x1, y1); // Move to the center of the rim
  rotate(-angle); // Rotate by the current angle
  fill("grey");
  rect(-7.5, -27.5, 15, 55, 10); // Vertical bar
  rect(-27.5, -7.5, 55, 15, 10); // Horizontal bar

  pop(); // Restore the drawing state
  
  angle += 0.03; // Increment angle for spinning effect
}







