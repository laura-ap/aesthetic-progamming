# MiniX1
###### Please run the code [here](https://aesthetic-progamming-laura-ap-480b211131f65acb83f176e1696bf281f.gitlab.io/MiniX1/index.html) <br>  Please view the full repository [here](https://gitlab.com/laura-ap/aesthetic-progamming/-/tree/main/MiniX1)

**What have you produced?** <br>
For my first MiniX i decided to draw a car. I have done this by using different geometric shapes and a reference image of a Ferrari Testarossa.

However, it was not the actual shape of the car that was my main focus in the task, but rather getting the wheels to spin.
I wanted to keep my code relatively simple, but I also decided early on in the process to challenge myself by having an element in motion. I played around with pre-made codes from the p5 library, but couldn't find any that made sense to use.
Therefor i created a function, that used the method ``rotate()`` to make the rims of the car spin, and the methods ``push()`` and ``pop()`` to save and restore my drawing.

For the background of my drawing, I wanted to use a neutral color, so I used grey, but for the actual car i used the rgb code ``(255, 40, 0)`` , which is an actual Ferrari red. <br> I made details, in forms of lines, with a darker red, and with the ``line()`` method.
<br>

**How is the coding process different from, or similar to, reading and writing text?** <br>
Personally, I feel like there is a big difference between coding and reading/writing. It immediately feels like there are many 'rules' in coding, and a small mistake can have a big impact on your results.
On the other hand, I think it's super cool when it works, and a different way of being creative. I'm sure you can accomplish a lot of things with it once you learn to understand and write it.<br>
As mentioned, I decided to keep my code relativly simple, and at a level where I actually understood what I was writing, while also looking at the refrences and examples from the p5 library. I feel like this approach suited me well, because i both learned something, and  didn't get intimidated.
<br>
<br>

![](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExczRkNWZwdGo4NnJwaHpmNHRzZ3NtcjIxaGFwOGN4OWQ3MTV6a3c4ayZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/9RDJEf9yWdKySXgUcH/giphy.gif)

